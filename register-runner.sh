#!/bin/bash
# Get the registration token from:
# http://localhost:8080/root/${project}/settings/ci_cd

if [[ -z "$1" ]]; then
  echo "Please specify the gitlab runner token asgument" >&2
  exit 1
fi

docker exec -it gitlab-runner \
  gitlab-runner register \
    --non-interactive \
    --registration-token "${1}" \
    --locked=false \
    --description docker-stable \
    --url http://gitlab-web/ \
    --executor docker \
    --docker-image docker:stable \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
    --docker-network-mode gitlab-network

